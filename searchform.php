<?php
$unique_id = wp_unique_id( 'search-form-' );

$aria_label = ! empty( $args['label'] ) ? $args['label'] : 'Search';
?>
<form role="search" aria-label="<?php echo esc_attr($aria_label); ?>" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label for="<?php echo esc_attr( $unique_id ); ?>">
        <span class="screen-reader-text"><?php esc_html_e( 'Search for:', 'weidenhammer' ); ?></span>
        <input type="search" id="<?php echo esc_attr( $unique_id ); ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'weidenhammer' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
    </label>
    <input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'GO', 'submit button', 'weidenhammer' ); ?>" />
</form>