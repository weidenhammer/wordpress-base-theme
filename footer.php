</main><!-- main -->

<button class="back-to-top icon-up" aria-label="Back to Top"></button>

<footer class="mt-50 pt-20 pb-20">
    <div class="footer container wide d-flex f-a-center f-j-center flex-wrap flex-wrap-sm">
        <p>Copyright &copy; <?php echo esc_html(date('Y')); ?> All Rights Reserved.</p>

        <?php
        // social
        hammer()->get_social_media();
        ?>
        <a class="site-by" href="https://www.hammermarketing.com" target="_blank" aria-label="Link to Hammer Marketing website">
            <?php echo get_template_part('assets/svg/footer.svg'); ?>
        </a>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>