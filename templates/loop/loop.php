<article <?php post_class(); ?>>
    <?php 
    $theID = get_the_ID();
    echo '<a class="img-wrap hover" href="'.esc_url(get_permalink()).'" title="'.esc_html(get_the_title()).'">';
    if ( has_post_thumbnail()) {
        $imgpath = get_the_post_thumbnail_url($theID, array(300,300));
    } else {
        $imgpath = get_stylesheet_directory_uri().'/assets/images/default-blog.jpg'; 
    }
    echo '<img src="'.esc_url($imgpath).'" alt="'.esc_attr(get_the_title($theID)).'" />';
    echo '</a>';
    ?>
    <div class="blog-content">
        <div class="post-meta mt-10 mb-10">
            <span class="date"><?php the_time('F j, Y'); ?></span>
            <span class="author"><?php esc_html_e( 'Published by', 'weidenhammer' ); ?> <?php the_author_posts_link(); ?></span>
        </div>

        <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>

        <p><?php echo esc_html(hammer()->ham_excerpt(20)); ?></p>

        <a class="btn" href="<?php the_permalink(); ?>">Continue Reading</a>
    </div>
</article>