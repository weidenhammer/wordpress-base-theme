<?php
if(!defined('ABSPATH')){ exit; } //Exit if accessed directly

define( 'HAM_THEME_DIR', trailingslashit( get_template_directory() ) );

// LETS SET THINGS UP
add_action('after_setup_theme', 'ham_theme_setup');
add_filter('body_class', 'ham_custom_body_classes');
add_action('template_redirect', 'check_for_hammer_class_frontend');

if ( function_exists('register_block_type') ){
    add_action('init', 'hammer_init');
}

function ham_theme_setup() {
    //* - - - - - - -
    /* CUSTOM HEADER
    */
    add_theme_support( 'custom-header', array(
        'width'            => 1600,
        'height'           => 250,
        'header-text'      => false,
        'random-default'   => true,
    ));

    //* - - - - - - -
    /* NAV MENUS
    */
    register_nav_menus( array(
        'primary'   => __('Primary Menu', 'weidenhammer' ),
        'secondary' => __('Secondary Menu', 'weidenhammer' ),
    ));
}

function hammer_init() {
    // checking if Hammer class is active (via the Hammer Custom plugin).
    if (!class_exists('Hammer')) {
        add_action('admin_notices', 'hammer_class_warning');
    }

    //* - - - - - - -
    /* GUTENBERG CUSTOM CLASSES
    // eventually theme.json will house this
    */
    register_block_style('core/list', [
        'name' => 'two-column',
        'label' => 'Two Columns',
    ]);
    register_block_style('core/button', [
        'name' => 'alt',
        'label' => 'Alt',
    ]);
}

//* - - - - - - -
/* CUSTOM BODY CLASSES
*/
function ham_custom_body_classes( $classes ) {
    if (is_singular()) :
        $classes[] = 'singular';
        // ADD MORE HERE
    endif;

    return $classes;
}

//* - - - - - - - - - - - - - - - -
/* ERROR CHECKING FOR CUSTOM PLUGIN
*/
function hammer_class_warning() {
    ?>
    <div class="notice notice-warning">
        <p>Please install the Hammer Custom plugin.</p>
    </div>
    <?php
}

function check_for_hammer_class_frontend() {
    if (!class_exists('Hammer') || !class_exists('hamCustom') ) {
        wp_die(
            __('Please install the Hammer Custom plugin.', 'hammer'),
            __('Missing Dependency', 'hammer'),
            array('back_link' => true)
        );
    }
}

// loading assets
require_once(HAM_THEME_DIR.'inc/Assets.php');
// custom menu additions / walkers
require_once(HAM_THEME_DIR.'inc/Menus.php');
// custom functions
require_once HAM_THEME_DIR . 'inc/Functions_Custom.php';
// locations - UNCOMMENT IF USING
// require_once HAM_THEME_DIR . 'inc/Locations/Locations.php';