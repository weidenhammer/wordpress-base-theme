<?php 
get_header();
hamCustom()->get_header_images();

global $wp_query;
    if(is_home()) {
        get_template_part( 'templates/archive/archive' );
    } elseif(is_search()) {
        get_template_part( 'templates/archive/archive', 'search' );
    } else {
        if(is_tax()) {
            $tax = $wp_query->query_vars['taxonomy'];
            get_template_part( 'templates/archive/taxonomy', $tax );
        }
        if(is_category()) {
            get_template_part( 'templates/archive/taxonomy', 'category' );
        }
        if(is_post_type_archive()) {
            $posttype = $wp_query->query['post_type'];
            get_template_part( 'templates/archive/archive', $posttype );
        }
    }
get_footer();