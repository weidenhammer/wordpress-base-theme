<?php 
get_header();
hamCustom()->get_header_images(); 
hammer()->ham_yoast_breadcrumbs();
?>

    <div class="entry-content">
        <?php 
        if(have_posts()): while(have_posts()): the_post();
            the_content();
        endwhile; endif;
        ?>
        </div>
    </div><!-- content -->

<?php get_footer();