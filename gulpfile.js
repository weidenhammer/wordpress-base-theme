const { src, dest, watch, series, parallel } = require('gulp');

const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const cssnano = require('cssnano');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const browserSync = require('browser-sync').create();

const onError = function (err) {
    notify.onError({
        title: "Gulp",
        subtitle: "Failure!",
        message: "Error: <%= error.message %>",
        sound: "Beep"
    })(err);
    this.emit('end');
};

// File paths
const files = {
    cssPath: './assets/scss/**/*.*ss',
    blockCssPath: './blocks/**/*.*ss',
    jsHeaderPath: './assets/js/modules/*.js',
    jsPath: './assets/js/partials/*.js',
}

// blocks css
function blocks() {
    return src('./blocks/**/*.scss')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()], { syntax: require('postcss-scss') }))
        .pipe(dest('./dist/blocks'))
        .pipe(notify('BLOCK CSS! (っ◕‿◕)っ'));
}

// normal css
function css() {
    return src('./assets/scss/style.scss')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()], { syntax: require('postcss-scss') }))
        .pipe(sourcemaps.write())
        .pipe(dest('./dist/css'))
        .pipe(notify('SCSS! (っ◕‿◕)っ'));
}

// editor css
function cssEditor() {
    return src('./assets/scss/style-editor.scss')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()], { syntax: require('postcss-scss') }))
        .pipe(sourcemaps.write())
        .pipe(dest('./'))
        .pipe(notify('Editor SCSS! ᕦ(ˇò_ó)ᕤ'));
}

// js
function js() {
    return src(files.jsPath, { sourcemaps: true })
        .pipe(plumber({ errorHandler: onError }))
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(dest('./dist/js'), { sourcemaps: true })
        .pipe(notify('JavaScript Footer! (⌐■_■)'));
}

// js header
function jsHeader() {
    return src(files.jsHeaderPath, { sourcemaps: true })
        .pipe(plumber({ errorHandler: onError }))
        .pipe(concat('header-scripts.min.js'))
        .pipe(uglify())
        .pipe(dest('./dist/js'), { sourcemaps: true })
        .pipe(notify('JavaScript Header! (⌐□_□)'));
}

function watchTask(){
    watch(files.cssPath,series(css));
    watch(files.blockCssPath,series(blocks));
    watch(files.cssPath,series(cssEditor));
    watch(files.jsHeaderPath,series(jsHeader));
    watch(files.jsPath,series(js));
}

exports.default = watchTask;