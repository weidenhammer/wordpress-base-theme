<?php
$class_name = 'block expandable';

if(!empty( $block['className'])) {
    $class_name .= ' ' . $block['className'];
}
if(!empty( $block['align'])) {
    $class_name .= ' align' . $block['align'];
}

$anchor = '';
if(!empty( $block['anchor'])) {
    $anchor = 'id="' . esc_attr( $block['anchor'] ) . '" ';
}

// ADD YOUR FIELDS
$pre = 'expandable_';
$title = get_field($pre.'title') ?: null;
$slug = get_field($pre.'slug') ?: null;
$open = get_field($pre.'open') ?: false;

$name = '';
if(!empty($slug)) :
    $name_clean = $slug;
    $name = ' name="'.esc_attr($name_clean).'"';
endif;

$opens = $open ? ' open' : null;

// BUILD THE BLOCK
echo '<details '.$anchor.' class="'.esc_attr($class_name).'"'.$name.$opens.'>';
    echo '<summary class="ham-expand-btn">'.esc_html($title).'</summary>';;
    echo '<InnerBlocks className="ham-expand-content" />';
echo '</details>';