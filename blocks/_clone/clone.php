<?php
// $is_preview -> checks if its the backend

// ADD YOUR CLASSNAME AFTER 'BLOCK'
$class_name = 'block';

if(!empty( $block['className'])) {
    $class_name .= ' ' . $block['className'];
}
if(!empty( $block['align'])) {
    $class_name .= ' align' . $block['align'];
}

$anchor = '';
if(!empty( $block['anchor'])) {
    $anchor = 'id="' . esc_attr( $block['anchor'] ) . '" ';
}

// ADD YOUR FIELDS
$pre = 'clone_';
$title = get_field($pre.'title') ?: 'Welcome';

// BUILD THE BLOCK
echo '<div '.$anchor.' class="'.esc_attr($class_name).'">';
    echo $title;
echo '</div>';