<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php
    echo get_template_part('inc/metadata');
    wp_head();
    ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
<header class="header-wrap">

  <div class="header container">

    <div class="logo">
        <a href="<?php echo esc_url(home_url()); ?>" title="<?php bloginfo('name'); ?>" aria-label="Website logo, link to homepage">
            <?php echo get_template_part('assets/svg/logo.svg'); ?>
        </a>
    </div>

    <!-- choose here - https://jonsuh.com/hamburgers -->
    <button class="hamburger hamburger--collapse" type="button" aria-label="Main Menu">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </button>

    <div class="nav-wrap">
        <?php
        echo '<nav aria-label="Primary Navigation">';
        if (has_nav_menu('primary')) {
            wp_nav_menu(
                array(
                    'theme_location' => 'primary',
                    'container'      => false,
                    'menu_class'     => 'main-nav nav',
                    'walker'         => new Texas_Ranger(),
                    'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                )
            );
        }
        echo '</nav>';

        // search
        echo '<div class="search-form-wrap">';
            get_search_form(
                array(
                    'label' => 'Search For...',
                )
            );
            echo '</div>';
            ?>
    </div>

    </div><!-- container -->

</header><!-- header -->

<main id="maincontent" class="content-overflow">