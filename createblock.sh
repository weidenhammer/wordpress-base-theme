#!/bin/bash

# Check if correct number of arguments are passed
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <block_name> <pretty_name>"
    exit 1
fi

# Parameters
BLOCK_NAME="$1"
PRETTY_NAME="$2"

# Directory where blocks will be created
BLOCKS_DIR="blocks"

# Create the folder if it doesn't exist
mkdir -p "$BLOCKS_DIR/$BLOCK_NAME"

# Create block.json file with initial content
cat << EOF > "$BLOCKS_DIR/$BLOCK_NAME/block.json"
{
    "name": "acf/$BLOCK_NAME",
    "title": "$PRETTY_NAME",
    "description": "A custom $PRETTY_NAME block",
    "style": [ "file:../../dist/blocks/$BLOCK_NAME/$BLOCK_NAME.css" ],
    "category": "design",
    "icon": "admin-comments",
    "acf": {
        "mode": "auto",
        "renderTemplate": "$BLOCK_NAME.php"
    },
    "supports": {
        "align": false,
        "anchor": true
    }
}
EOF

# Create blockname.php with initial content
cat << EOF > "$BLOCKS_DIR/$BLOCK_NAME/$BLOCK_NAME.php"
<?php
// \$is_preview -> checks if its the backend
\$class_name = 'block $BLOCK_NAME';

if(!empty( \$block['className'])) {
    \$class_name .= ' ' . \$block['className'];
}
// remove align if you don't need it
if(!empty( \$block['align'])) {
    \$class_name .= ' align' . \$block['align'];
}
// remove anchor if you don't need it
\$anchor = '';
if(!empty( \$block['anchor'])) {
    \$anchor = 'id="' . esc_attr( \$block['anchor'] ) . '" ';
}

\$pre = '$BLOCK_NAME' . '__';
\$title = get_field(\$pre.'title') ?: 'Welcome';

// BUILD THE BLOCK
echo '<div '.\$anchor.' class="'.esc_attr(\$class_name).'">';
    echo \$title;
echo '</div>';
EOF

# Create blockname.scss with initial content
cat << EOF > "$BLOCKS_DIR/$BLOCK_NAME/$BLOCK_NAME.scss"
@import "../../assets/scss/core/variables";
@import "../../assets/scss/core/mixins";

.block.$BLOCK_NAME {
  position:relative;
}
EOF

echo "Block '$PRETTY_NAME' created successfully in '$BLOCKS_DIR/$BLOCK_NAME'."