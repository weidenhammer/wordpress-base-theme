<?php 
/*
Template Name: Locations
 */
get_header();
    hamCustom()->get_header_images(); 
    hammer()->ham_yoast_breadcrumbs();
    ?>

    <div class="entry-content">
        <?php if(have_posts()): while(have_posts()): the_post(); ?>
        <div class="row">
            <div class="left col-xs-12 col-sm-8 col-md-9">
                <?php 
                $subtitle = get_post_meta(get_the_ID(), '_ham_custom_page_custom_subheading',true);

                if($subtitle) echo '<h2 class="subtitle">'.esc_html($subtitle).'</h2>';
                ?>
                <?php 
                the_content();

                get_template_part( 'inc/Locations/template', 'map' );
                ?>
            </div><!-- left -->
                
            <div class="sidebar col-xs-12 col-sm-4 col-md-3" role="complementary">
                <?php dynamic_sidebar('primary'); ?>
            </div><!-- sidebar -->
    
        <?php endwhile; endif; ?>
        </div>
    </div><!-- content -->

<?php get_footer(); ?>