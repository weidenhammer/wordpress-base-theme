<?php
if (!defined('ABSPATH')){ die(); } //Exit if accessed directly

class Texas_Ranger extends Walker_Nav_Menu {

    // Add has-children class to parent nav items and then let the parent function do the rest
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
        if ( is_object( $args[0] ) ) {
            $args[0]->item_title = $element->title;
            $args[0]->item_url = $element->url;
        }
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $thumbnail = get_field('menu_image',$item->ID);
        if( $thumbnail ) {
            $item->classes[] = 'image-link';
           $args->link_before = wp_get_attachment_image($thumbnail['ID'], 'thumbnail') . '<span class="link-text"><em>';
           $args->link_after = '</em> '.$item->description.'</span>';
        } else {
            $args->link_before = '';
            $args->link_after = '';
        }

        parent::start_el($output, $item, $depth, $args);
        if(!empty($item->description)) $output .= '<p class="desc">'.$item->description.'</p>';
    }
}