<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" >
<link rel="profile" href="https://gmpg.org/xfn/11">

<link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/fonts/hammer.woff2" as="font" type="font/woff2" crossorigin>

<?php
// output fonts
echo get_field('hammer_setting_fonts','option');

/* - - - - - - - - - - - - - - - - - - - - - - - - - -
/* COLOR SETTINGS
*/
$ham_colors = hammer()->ham_get_colors();

$color_pre = 'hammer_setting_colors_';

$global_padding = get_field('hammer_setting_padding','option') ?: '20';
?>
<style>
:root { 
<?php 
foreach($ham_colors as $color) :
    echo '--color_'.$color[0].':'.$color[1].';'."\n";
endforeach;
?>
--color_primary_rgb: <?php echo esc_attr(hammer()->hex2rgb($ham_colors[0][1])); ?>;
--color_secondary_rgb: <?php echo esc_attr(hammer()->hex2rgb($ham_colors[1][1])); ?>;
--color_gray_rgb: <?php echo esc_attr(hammer()->hex2rgb($ham_colors[4][1])); ?>;
--hammer_global__padding: <?php echo esc_attr($global_padding); ?>rem;
}
</style>
<?php
do_action('hammer_metadata_end');