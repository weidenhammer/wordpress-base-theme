<?php
$theID = get_the_ID();
$address = get_post_meta(get_the_ID(), '_ham_locations_address',true);

echo get_the_title($theID);
echo '<address>';
    echo esc_html($address['address-1']);
    echo '<br />';
    echo esc_html($address['city']).' '.esc_html($address['state']).' '.esc_html($address['zip']);

    echo wpautop(get_the_content($theID));
echo '</address>';