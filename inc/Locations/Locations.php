<?php
if ( !defined('ABSPATH') ){ die(); } //Exit if accessed directly

if ( !class_exists('HamLocations') ) :
    class HamLocations {
        private static $instance;

        public $p = "_ham_";
        // post type
        public $post_type = "locations";
        public $post_type_rewrite = "locations";
        public $pretty_name = "Location";
        public $pretty_name_plural = "Locations";

        public static function get_instance() {
            if ( ! isset( self::$instance ) ) {
                self::$instance = new self();
            }
            return self::$instance;
        }

        public function __construct() {
            add_action( 'init', array($this,'locations_register_posttype') );
            add_action( 'acf/init', array($this, 'register_location_fields') );

            add_action('wp_ajax_map_info_window', array($this,'get_map_location'));
            add_action('wp_ajax_nopriv_map_info_window', array($this,'get_map_location'));
        }

        public function locations_register_posttype(){
            register_post_type( $this->post_type,
                array(
                    'labels'            => array(
                        'menu_name'     => $this->pretty_name_plural,
                        'name'          => $this->pretty_name,
                        'add_new_item'  => 'Add New '.$this->pretty_name,
                        'singular_name' => $this->pretty_name,
                        'search_items'  => 'Search '.$this->pretty_name_plural,
                        'edit_item'     => 'Edit '.$this->pretty_name
                    ),
                    'description'   => $this->pretty_name.' Content',
                    'hierarchical'  => true,
                    'public'        => true,
                    'exclude_from_search' => false,
                    'has_archive'   => true,
                    'show_in_rest'  => true,
                    'menu_icon'     => 'dashicons-palmtree',
                    'supports'      => array('title', 'editor', 'revisions'),
                    'rewrite'       => array( 'slug' => $this->post_type_rewrite ),
                )
            );
        }

        // META BOXES
        public function register_location_fields(){
            if( ! function_exists('acf_add_local_field_group') )
                return;
            acf_add_local_field_group(array(
                'key' => 'custom_'.$this->post_type.'_meta_fields_acf',
                'title' => $this->pretty_name.' Fields',
                'fields' => array(
                    array(
                        'key' => $this->p.$this->post_type.'_address',
                        'label' => 'Address',
                        'name' => 'address',
                        'type' => 'google_map',
                        'instructions' => '',
                        'center_lat' => '-37.81411',
                        'center_lng' => '144.9635',
                        'zoom' => 12,
                        'height' => 500,
                    ),
                    array(
                        'key' => $this->p.$this->post_type.'_phone',
                        'label' => 'Phone Number',
                        'name' => 'phone',
                        'type' => 'text',
                    ),
                    array(
                        'key' => $this->p.$this->post_type.'_headquarters',
                        'label' => 'Headquarters',
                        'name' => 'headquarters',
                        'type' => 'true_false',
                        'message' => 'Is this the headquarters?',
                        'default_value' => 0,
                        'ui' => 1,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => $this->post_type,
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'left',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'show_in_rest' => 0,
            ));
        }

        public function get_location_array() {
            $locations = new WP_Query(array(
                'post_type' => 'locations',
                'posts_per_page' => -1,
                'orderby' => 'menu_order',
                'order' => 'ASC'
            ));
            if($locations->have_posts() ) :
                while ( $locations->have_posts() ) : $locations->the_post();
                    $theID = get_the_ID();

                    $address = get_post_meta($theID, '_ham_locations_address', true);
                    $headquarters = get_post_meta($theID, '_ham_locations_headquarters', true);
                    $phone = get_post_meta($theID,'_ham_locations_phone',true);

                    $loc_array[] = array(
                        'lat' => $address['lat'],
                        'lng' => $address['lng'],
                        'post_id'  => $theID,
                    );
                endwhile; wp_reset_query(); 
                    
                return $loc_array;
            endif;
        } // get location array

        public function get_map_location() {
            if( !isset($_POST['post_id']) || !is_numeric($_POST['post_id'])) wp_send_json_error('No post ID.');
            $html = '';
            
            $loc_query = new WP_Query(array(
                'post_type' => $this->post_type,
                'p' => $_POST['post_id']
            ));
            if( $loc_query->have_posts() ): while( $loc_query->have_posts() ): $loc_query->the_post();
                ob_start();
                get_template_part('inc/Locations/template','popup');
                $html .= ob_get_contents();
                ob_end_clean();
            endwhile; else:
                wp_send_json_error('Post not found');
            endif;

            wp_send_json_success($html);
            // echo wp_json_encode(array('html'=>$html));
            die();
        }
    } // class

    HamLocations::get_instance();

endif; // if class