/* global google, map_data */
var hamMaps = {
    map : false,
    bounds : false,
    geocoder : false,
    infowindow : false,
    map_div : document.getElementById("locations-map"),

    init: function() {
        var _self = this;
        _self.buildMap();
    },

    buildMap: function() {
        var _self = this;

        // bail if needed
        if( typeof google === 'undefined' || _self.map_div === null ) return false;

        // map options
        var map_options = {
            zoom: 10,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{"featureType":"landscape","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]}]
        }

        _self.infowindow = new google.maps.InfoWindow({
            size:      new google.maps.Size(300, 160),
            content:   'loading',
            maxWidth:  380,
            maxHeight: 500
        });

        // build the map
        _self.map = new google.maps.Map(document.getElementById("locations-map"),map_options);

        _self.bounds = new google.maps.LatLngBounds();
        _self.geocoder = new google.maps.Geocoder();
        _self.addLocations();

        return true;
    },

    addLocations: function() {
        var _self = this;
        var locations = map_data.locations;

        _self.markers = new Array();
        if(!locations) return;

        for(var i in locations) {
            var loc = locations[i];
            var latln = false;
            var marker = false;

            // If latitude and longitude are not set move on.
            if(!parseFloat(loc.lat) || !parseFloat(loc.lng)) continue;

            ltln = new google.maps.LatLng(loc.lat, loc.lng);
            marker = new google.maps.Marker({
                draggable: false,
                position:  ltln,
                map:       _self.map,
                post_id:   loc.post_id,
                icon: bloginfo.template_url + '/inc/Locations/images/pin.png'
            });
            _self.bounds.extend(ltln);
            _self.eventListeners(marker);
            _self.markers.push(marker);
        }

        _self.map.fitBounds(_self.bounds);
    },

    eventListeners: function(marker) {
        var _self = this;

        google.maps.event.addListener(marker, 'click', function() {
            _self.map.panTo(marker.getPosition());

            _self.infowindow.close();
            _self.infowindow.setContent('<img src="'+bloginfo.template_url+'/inc/Locations/images/loader.gif" class="map-loader" />');
            _self.infowindow.open(_self.map,marker);
            _self.getContent(marker.post_id, _self);

        });

        google.maps.event.addListener(_self.map, 'center_changed', function() {
            _self.limitBounds(_self.map);
        });

        google.maps.event.addListener(_self.map, "rightclick", function(e) {
            var mpi = {};
            mpi.center = _self.map.getCenter().toString();
            mpi.lat = e.latLng.lat();
            mpi.lng = e.latLng.lng();
        });
    },

    getContent: function(post_id, _self) {
        jQuery.post(bloginfo.ajax_url, {
            action:  'map_info_window',
            post_id: post_id,
        }, function(result) {
            console.log('Result',result);
            if(result.success) {
                _self.infowindow.setContent(result.data);
            } else {
                _self.infowindow.setContent('There was an error.');
            }
        });
    },

    limitBounds: function(map) {
        var latNorth = map.getBounds().getNorthEast().lat();
        var latSouth = map.getBounds().getSouthWest().lat();
        var newLat;

        if( (latNorth<85 && latSouth>-85) || (latNorth>85 && latSouth<-85) ) {
            return;
        } else {
            if(latNorth>85)
                newLat =  map.getCenter().lat() - (latNorth-85);   /* too north, centering */
            if(latSouth<-85)
                newLat =  map.getCenter().lat() - (latSouth+85);   /* too south, centering */
        }

        if(newLat) {
            var newCenter= new google.maps.LatLng( newLat ,map.getCenter().lng() );
            map.setCenter(newCenter);
        }

    },
}

var mapInit = function() {
    hamMaps.init();
};