<?php
$api = get_field('google_api_key','option') ?: '';
$locations = new WP_Query(array(
    'post_type' => 'locations',
    'posts_per_page' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC'
));
if($locations->have_posts() ) : ?>

<style>
    #locations-map {
        padding-bottom:45%;
        background:gray;
        margin:3rem 0;
    }
</style>

<script defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo esc_attr($api); ?>&callback=mapInit"></script>

<div class="locations">
    <div id="locations-map"></div>
    <ul class="locations-list">
        <?php 
        while ( $locations->have_posts() ) : $locations->the_post();
            $theID = get_the_ID();

            $address = get_post_meta($theID, '_ham_locations_address', true);
            $headquarters = get_post_meta($theID, '_ham_locations_headquarters', true);
            $phone = get_post_meta($theID,'_ham_locations_phone',true);

            echo '<li>';
                echo '<h6>'.esc_html(get_the_title($theID)).'</h6>';
                
                if($headquarters == 'on'):
                    echo '<strong>(Headquarters)</strong>';
                endif;

                echo '<address>';
                        echo esc_html($address['pretty_address']) . '<br />';

                        $encoded = urlencode($address['address-1'].' '.esc_html($address['city']).' '.esc_html($address['state']).' '.esc_html($address['zip']));
                echo '</address>';
                
                if($phone): 
                    echo '<span><strong>Phone:</strong>'.esc_html($phone).'</span>';
                endif;

                echo '<a href="https://www.google.com/maps/dir/'.esc_attr($encoded).'/" class="btn-directions" target="_blank">Get Directions</a>';
            echo '</li>';
        endwhile; wp_reset_query(); 
        ?>
    </ul>
</div>
<?php endif;