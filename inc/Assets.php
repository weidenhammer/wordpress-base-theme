<?php
if ( !defined('ABSPATH') ){ die(); } //Exit if accessed directly


//Register styles/scripts
add_action('init', 'registerScripts');

//Enqueue styles/scripts
add_action('wp_enqueue_scripts', 'hamEnqueueScripts');
// block editor js
add_action('enqueue_block_editor_assets','ham_gutenberg_scripts');

/* - - - - - - - - - - - - -
/* REGISTER SCRIPTS / STYLES
*/
function registerScripts(){
    // this is to register the script/style, and then call it later dynamically (if needed) in enqueue_scripts

    $theme_version = wp_get_theme()->get( 'Version' );

    //Stylesheets
    wp_register_style( 'hammer-style', get_template_directory_uri().'/dist/css/style.css', array(), $theme_version );
    wp_register_style( 'hammer-print-style', get_template_directory_uri() . '/print.css', null, $theme_version, 'print' );
}

/* - - - - - - - - - - - - 
/* ENQUEUE SCRIPTS / STYLES
*/
function hamEnqueueScripts($hook){
    global $wp_query;
    
    $theme_version = wp_get_theme()->get( 'Version' );

    //Stylesheets
    wp_enqueue_style('hammer-style');
    wp_enqueue_style('hammer-print-style');

    // if there is a better spot / place for this - lets move it
    wp_enqueue_script( 'jquery' );

    if (is_page_template( 'page-templates/page_locations.php' ) ) :
        // register
        wp_register_script( 'ham_locations_script', get_template_directory_uri() . '/inc/Locations/locations-scripts.js', false, '1.0.0', true );
        wp_enqueue_script( 'ham_locations_script' );
        // localize
        wp_localize_script( 'ham_locations_script', 'map_data', array('locations' => $this->get_location_array())
        );
    endif;

    if ( !is_admin() ) {
        wp_enqueue_script( 'hammer-header', get_template_directory_uri().'/dist/js/header-scripts.min.js' );
        wp_enqueue_script( 'hammer-footer', get_template_directory_uri().'/dist/js/scripts.min.js', 'jquery', $theme_version, true );
        wp_localize_script( 'hammer-header', 'bloginfo', array(
            'url'           => home_url(),
            'template_url'  => get_stylesheet_directory_uri(),
            'ajax_url'      => admin_url('admin-ajax.php'),
            'posts'         => json_encode( $wp_query->query_vars ),
            'current_page'  => $wp_query->query_vars['paged'] ? $wp_query->query_vars['paged'] : 1,
            'max_page'      => $wp_query->max_num_pages
        ));
    }
}

/* - - - - - - - - -
/* GUTENBERG SCRIPTS
*/
function ham_gutenberg_scripts() {
    wp_enqueue_script(
        'ham_gutenberg_editor', 
        get_stylesheet_directory_uri() . '/dist/js/editor.js', 
        array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' )
    );
}