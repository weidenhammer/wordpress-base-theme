jQuery(function($){
    // MOBILE ONLY
    if(Modernizr.mq('only screen and (max-width: 768px)')) {
        $('.nav-wrap .main-nav > li').each(function(){
            if( $(this).find('> .sub-menu').length ) {
                var opener = $(this).find('> a').clone().empty().addClass('opener');
                $(this).append(opener);
                $(this).find('> a.opener').on('click',function(e){
                    e.preventDefault();
                    $(this).parent().siblings().removeClass('open');
                    $(this).parent().toggleClass('open');
                }).next('.sub-menu').prepend($('<li/>'));
            }

        });
    }

    // DESKTOP ONLY
    if(Modernizr.mq('only screen and (min-width: 769px)')) {
        $(window).scroll(function(){
            if ($(window).scrollTop() >= 100) {
                $('body').addClass('sticky');
            } else {
                $('body').removeClass('sticky');
            }
        });
    }

    // WRAPS BUTTONS WITH A SPAN
    $('.wp-block-button__link').wrapInner('<span></span>');

    // SEARCH
    $('.main-nav .search-toggle').on('click', function(e) {
        e.preventDefault();
        $('html').toggleClass('search-open');
        $(this).toggleClass('active');
    });

    function ajaxFilter() {
        var url = window.location.href,
            urlParts = url.split('?');

        $('.archive-filter').on('click','button',function(e){
            e.preventDefault();
            var theData = '',
                results = $('.results-wrap .ajax-results'),
                loadMore = $('#feed_load_more'),
                postType = $(this).data('type');
            
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                theData = 'all';

                if (urlParts.length > 0) {
                    var baseUrl = urlParts[0];
                    window.history.replaceState({}, null, baseUrl);
                }
            } else {
                theData = $(this).data('cat');
                $(this).addClass('active').siblings().removeClass('active');

                if (urlParts.length > 0) {
                    var baseUrl = urlParts[0];
                    window.history.replaceState({}, null, baseUrl + '?cat='+theData);
                }
            }

            hammerscripts.ajaxSimple('ham_filter_ajax',theData,postType,results,loadMore);
        });
    }

    ajaxFilter();
});