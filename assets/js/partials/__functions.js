var hammerscripts = {};
jQuery(function($){
    /* - - - - - - - - - - - - - - - - - - - - - - - - - -
    /* HAMBURGER
    */
    $('header .hamburger').on('click', function(e) {
        e.preventDefault();
        $('html').toggleClass('nav-open');
        $(this).toggleClass('is-active');

        $(this).attr('aria-expanded', function(i, attr) {
            return attr == 'true' ? 'false' : 'true'
        });
    });

    /* - - - - - - - - - - - - - - - - - - - - - - - - - -
    /* GETURLVARS
    // function to get the url and split it up by query strings (?'s)
    */
    hammerscripts.getUrlVars = function() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - -
    /* SOCIAL SHARING NEW WINDOWS
    */
    $('.social-share .nw').click(function(){
        var left  = ($(window).width() / 2) - (900 / 2),
            top   = ($(window).height() / 2) - (600 / 2);
        window.open($(this).attr('href'), "Share!", "width=900, height=600, top=" + top + ", left=" + left);
        return false;
      });

    /* - - - - - - - - - - - - - - - - - - - - - - - - - -
    /* SLIDER STUFF - https://swiperjs.com
    */
   
    $('.wp-block-gallery').each(function(i){
         $(this).wrap("<div class='wp-block-gallery-wrap'></div>");
        $(this).wrapInner( "<div class='swiper-wrapper'></div>");
        $(this).append('<button class="styled-btn gallery-button-prev"><</button><button class="styled-btn gallery-button-next">></button>');
    });

    setTimeout(function() {
        var gutenberg_swiper = new Swiper('.wp-block-gallery', {
        loop: true,
        slideClass: 'wp-block-image',
        slidesPerView: 1,
        centeredSlides:true,
        spaceBetween: 20,
        breakpoints: {
          768: {
            slidesPerView: 1.8,
          },
        },
        navigation: {
          nextEl: '.gallery-button-next',
          prevEl: '.gallery-button-prev',
        },
      })
    }, 50);
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - -
    /* MICROMODAL - https://micromodal.now.sh/
    */
   
   // example of trigger one specifically:
   // MicroModal.show('ID');
   // MicroModal.close('ID');
   
    MicroModal.init({
      // onShow: modal => console.info(`${modal.id} is shown`),
      // onClose: modal => console.info(`${modal.id} is hidden`),
      // openTrigger: 'data-custom-open',
      // closeTrigger: 'data-custom-close',
      // openClass: 'is-open',
      // disableScroll: true,
      // disableFocus: false,
      // awaitOpenAnimation: false,
      // awaitCloseAnimation: false,
      // debugMode: true
    });

    /* - - - - - - - - - - - - - - - - - - - - - - - - - -
    /* BACK TO TOP
    */
    var backtotop = $('.back-to-top');
    $(window).scroll(function() {
        if ($(window).scrollTop() > 900) {
            backtotop.addClass('show');
        } else {
            backtotop.removeClass('show');
        }
    });
    backtotop.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '400');

        $('.skip-to-content').focus();
    });

    /* - - - - - - - - - - - - - - - - - - - - - - - - - -
    /* AJAX FILTER
    functionName = the php function name that gets passed into the data:action
    
    queryVar = post type that's being passed into the data:query
  
    postType = the post type

    results = results wrap div / i.e. - results = $('.results-wrap .ajax-results')

    loadMore = the load more button ID. Set to false if not using the loadMore
    */
    hammerscripts.ajaxSimple = function(functionName,queryVar,postType='post',results,loadMore) {
        var loaderWrap = $('.results-wrap .loader-wrap');
      
        $.ajax({
            type:'post',
            url:bloginfo.ajax_url,
            dataType: 'json',
            data: {
                action:functionName,
                query:queryVar,
                post_type:postType
            },
            beforeSend: function(xhr){
                loaderWrap.addClass('loading');
            },
            success:function(data){
                if(data) {         
                    var html = data.content;

                    bloginfo.current_page = 1;
                    bloginfo.posts = data.posts;
                    bloginfo.max_page = data.max_page;

                    if(html != ''){
                        setTimeout(function () {
                            results.html(html);
                            loaderWrap.removeClass('loading');
                        }, 1200);
                    }

                    //if the load more button exists
                    if(loadMore.length > 0) {
                        if ( data.max_page < 2 ) {
                            loadMore.hide();
                        } else {
                            loadMore.show();
                        }
                    }
                }
            },
        });
    } // ajaxSimple
});

;( function( $, window, document, undefined ) {
    $( '.gfield.fileupload input' ).each( function() {
        var $input   = $( this ),
            $label   = $input.closest('.gfield').find('.gfield_label'),
            labelVal = $label.html();

        $input.on( 'change', function( e ) {
            var fileName = '';

            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else if( e.target.value )
                fileName = e.target.value.split( '\\' ).pop();

            $label.html( fileName );
        });

        // Firefox bug fix
        $input
        .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
        .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
    });
})( jQuery, window, document );