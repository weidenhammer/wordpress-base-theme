// CUSTOMIZED FROM THE BELOW AUTHOR:
// Author:      Equalize Digital
// Author URI:  https://equalizedigital.com
// License:     GPL-2.0+
// License URI: http://www.gnu.org/licenses/gpl-2.0.txt

(function ($) {
  "use strict";

  var ham_link_tooltip;

  function initializeTooltip() {
      ham_link_tooltip = $('<div/>').css({
          position: 'absolute',
          background: 'white',
          color: '#1e1e1e',
          fontSize: '16px',
          border: '1px solid black',
          padding: '5px 10px',
          zIndex: 9999,
          display: 'none'
      }).addClass('anww-tooltip').appendTo('body');
  }

  function processNewWindowLinks() {
      var ham_label = '';
      var opens_a_new_window = 'Opens in a new window';

      // Remove previously appended icons to avoid duplication
      $(".external-link-icon").remove();

      $("a[target=_blank]").each(function () {
          // Add icon to link
          // remove this if you don't want the icon
          if ($(':header', this).length) {
              $(':header', this).append('<i class="external-link-icon" aria-hidden="true"></i>');
          } else {
              $(this).append('<i class="external-link-icon" aria-hidden="true"></i>');
          }

          // Get aria label text
          if ($(this).attr("aria-label")) {
              ham_label = $(this).attr("aria-label");
          } else if ($('img', $(this)).length) {
              ham_label = $(this).find("img").attr("alt");
          } else if ($(this).text()) {
              ham_label = $(this).text();
          }

          // Add warning label
          if (ham_label) {
              ham_label = ham_label.trimEnd();
              ham_label += ", " + opens_a_new_window;
          } else {
              ham_label += opens_a_new_window;
          }

          // Add aria-label to link 
          $(this).attr("aria-label", ham_label);

          // Position and show link_tooltip on hover
          $(this).mousemove(function (e) {
              ham_link_tooltip.css({
                  top: e.pageY + 10 + 'px',
                  left: e.pageX + 10 + 'px'
              });
          })
          .hover(function () {
              ham_link_tooltip.show().html(opens_a_new_window);
          }, function () {
              ham_link_tooltip.hide();
          });

          // Position and show link_tooltip on focus
          $(this).on({
              focusin: function () {
                  var position = $(this).offset();
                  ham_link_tooltip.css({
                      top: position.top + $(this).outerHeight() + 'px',
                      left: position.left + 'px'
                  });

                  ham_link_tooltip.show().html(opens_a_new_window);
              },
              focusout: function () {
                  ham_link_tooltip.hide();
              }
          });
      });
  }

  $(window).on('load', function () {
      initializeTooltip();
      processNewWindowLinks();
  });

})(jQuery);